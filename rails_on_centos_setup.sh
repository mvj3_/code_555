# 创建用户
USERNAME=yourname
useradd $USERNAME
passwd $USERNAME

# 重新登陆
ssh-keygen
cd ~/.ssh
touch authorized_keys
chmod 600 authorized_keys

# 编辑/etc/profile，设置公共配置
alias ll="ls -l"
alias la="ls -a"
set -o vi
export PATH=/usr/sbin:/sbin:/bin:/usr/local/sbin/:/usr/local/bin:$PATH
export RAILS_ENV=production

# 安装相关程序和库
echo insecure >> ~/.curlrc
sudo yum install ruby gcc-c++ patch readline readline-devel zlib \
zlib-devel libyaml-devel libffi-devel openssl-devel make bzip2 \
autoconf automake libtool bison iconv-devel ntp irb \
mysql mysql-devel sqlite sqlite-devel mysql-server \
libevent libevent-devel -y

# 配置时区
cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
# sudo crontab -l
*/30 * * * *  ntpdate time-a.nist.gov
# 测试一下，看看是否输出为当前时间
date

# 安装Ruby相关，参考 http://ruby-china.org/wiki/install_ruby_guide，安装rvm及里面的readline

# 配置MySQL，参考http://dev.antoinesolutions.com/mysql
sudo chkconfig --levels 235 mysqld on
sudo /etc/init.d/mysqld start
mysql -u root # SET PASSWORD FOR 'root'@'localhost' = PASSWORD('new-password'); SET PASSWORD FOR 'root'@'localhost.localdomain' = PASSWORD('new-password'); SET PASSWORD FOR 'root'@'127.0.0.1' = PASSWORD('new-password');


# 安装Python版本管理器
# https://github.com/utahta/pythonbrew
# 安装pygments
pip install pygments


# 安装Redis相关
# tcl8.5.10
wget ftp://mirror.ovh.net/gentoo-distfiles/distfiles/tcl8.5.10-src.tar.gz
cd unix && ./configure && make && sudo make install
# redis && https://github.com/antirez/redis-tools
wget http://redis.googlecode.com/files/redis-2.4.14.tar.gz
make && make test && sudo make install

# memcached http://memcached.googlecode.com/
./configure && make && make test && sudo make install

# node.js http://nodejs.org/download/
# https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager
./configure && make && make test && sudo make install
sudo cp /usr/local/bin/node /usr/loca/bin/
sudo yum install nodejs-devel.x86_64 qcairo-devel.x86_64 libjpeg-devel.x86_64 -y
sudo cp /usr/local/bin/node-waf /usr/bin/


# mongodb http://www.mongodb.org/display/DOCS/CentOS+and+Fedora+Packages
sudo yum install mongo-10gen mongo-10gen-server -y
# 1.8升级到2.0
cd /usr/bin; ruby -e '`ls mongo*`.split.reject {|s| s.index(".") }.each {|m| `sudo cp #{m} #{m}.#{Time.now.strftime("%Y%m%d")}` }' # 重命名mongodb相关二进制命令
sudo yum remove mongo18-10gen.x86_64 mongo18-10gen-server.x86_64 -y
sudo yum install mongo-10gen mongo-10gen-server -y


# 安装git
wget https://github.com/git/git/archive/v1.8.2-rc3.zip
make configure && make && make test && sudo make install
